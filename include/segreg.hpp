#ifndef EMERGENCE_HH
#define EMERGENCE_HH

#include <levmar.h>

void segmented(double *, double *, int, int, void *);
void jacsegmented(double *, double *, int, int, void *);
void calc_residuals (std::vector<double>&,
                     std::vector<double>&,
                     std::vector<double>&,
                     std::vector<double>&);
void grid_search(std::vector<double> &,
                 std::vector<double> &,
                 double&,
                 std::vector<double> &,
                 std::vector<double> &,
                 std::vector<double> &,
                 unsigned int,
                 unsigned int);
void grid_search(std::vector<double> &,
                 std::vector<std::vector<double> > &,
                 std::vector<double> &,
                 std::vector<std::vector<double> > &,
                 std::vector<std::vector<double> > &,
                 std::vector<std::vector<double> > &,
                 unsigned int,
                 unsigned int);
void create_bp_vector(unsigned int, std::vector<double> const &,
                      std::vector<double>&, unsigned int);
std::string print_results(double const&,
                          std::vector<double> const &,
                          std::vector<double> const &,
                          std::vector<double> const &,
                          std::vector<double> const &,
                          std::vector<double> const *
                          );
std::string print_results(std::vector<double> const &,
                          std::vector<std::vector<double> > const &,
                          std::vector<std::vector<double> > const &,
                          std::vector<std::vector<double> > const &,
                          std::vector<double> const &,
                          std::vector<std::vector<double> > const *
                          );

void segmented(double *p, double *x, int m, int n, void *adata) {
  double *data = (double*) adata;
  int num_breakpoints = data[0];
  double *t, *bp;
  t = (double *) malloc (n * sizeof(double));
  bp = (double *) malloc (num_breakpoints * sizeof(double));
  
  register int i, j;

  for (i = 0; i < num_breakpoints; ++i) {
    bp[i] = data[i+1];
  }
    
  for (i = 0; i < n; ++i) {
    t[i] = data[i+1+num_breakpoints];
  }

  for (i = 0; i < n; ++i) {
    x[i] = p[0] + p[1] * t[i];

    for (j = 0; j < num_breakpoints; ++j) {
      if (i > bp[j]) {
        x[i] += p[j+2] *
          (t[i] - t[static_cast<unsigned int>(bp[j])]);
      }
    }
  }

  free(t);
  free(bp);
}

void jacsegmented(double *p, double *d, int m, int n, void *adata)
{
  double *data = (double*) adata;
  int num_breakpoints = data[0];
  double *t, *bp;
  t = (double *) malloc (n * sizeof(double));
  bp = (double *) malloc (num_breakpoints * sizeof(double));

  register int i, j;

  for (i = 0; i < num_breakpoints; ++i) {
    bp[i] = data[i+1];
  }
    
  for (i = 0; i < n; ++i) {
    t[i] = data[i+1+num_breakpoints];
  }
    
  for (i = 0; i < n; ++i) {
    d[i*m+0] = 1;
    d[i*m+1] = t[i];
    for (j = 0; j < num_breakpoints; ++j) {
      if (i > bp[j]) {
        d[i*m+j+2] = t[i] - t[static_cast<unsigned int>(bp[j])];
      } else {
        d[i*m+j+2] = 0;
      }
    }
  }

  free(t);
  free(bp);
}

// subroutine to calculate residuals from breakpoints, paramters and original
// data
void calc_residuals (std::vector<double>& current_bp,
                     std::vector<double>& p,
                     std::vector<double>& data,
                     std::vector<double>& residuals)
{
  residuals.clear();
  unsigned int data_idx = static_cast<unsigned int>(current_bp[0]);
  for (unsigned int i = 0; i < data.size(); ++i) {
    double model_y = p[0] + p[1] * current_bp[1 + data_idx + i];
    for (unsigned int j = 0; j < data_idx; ++j) {
      // loop over all breakpoints
      if (i > current_bp[1+j]) {
        model_y += p[2+j] * (current_bp[1 + data_idx + i] -
                             current_bp[1 + data_idx +
                                        static_cast<unsigned int>(current_bp[1+j])]); 
      }
    }
    residuals.push_back(model_y - data[i]);
  }
}

// perform the grid search
void grid_search(std::vector<double> &current_bp,
                 std::vector<double> &data,
                 double& bestQ,
                 std::vector<double> &best_bp,
                 std::vector<double> &best_residuals,
                 std::vector<double> &best_parameters,
                 unsigned int minYears,
                 unsigned int verbose)
{
  std::vector<std::vector<double> > vData(1, data);
  std::vector<double> vBestQ(1, bestQ);
  std::vector<std::vector<double> > vBest_bp(1, best_bp);
  std::vector<std::vector<double> > vBest_residuals(1, best_residuals);
  std::vector<std::vector<double> > vBest_parameters(1, best_parameters);

  grid_search(current_bp, vData, vBestQ, vBest_bp, vBest_residuals,
              vBest_parameters, minYears, verbose);

  bestQ = vBestQ[0];
  best_bp = vBest_bp[0];
  best_residuals = vBest_residuals[0];
  best_parameters = vBest_parameters[0];
}

void grid_search(std::vector<double> &current_bp,
                 std::vector<std::vector<double> > &data,
                 std::vector<double>& bestQ,
                 std::vector<std::vector<double> > &best_bp,
                 std::vector<std::vector<double> > &best_residuals,
                 std::vector<std::vector<double> > &best_parameters,
                 unsigned int minYears,
                 unsigned int verbose)
{
                 
  unsigned int data_idx = static_cast<int>(current_bp[0]);

  if (verbose >= 2) {
    std::cout << "BP ";
    for (unsigned int j = 1; j < data_idx + 1; ++j) {
      std::cout << current_bp[j] << " ";
    }
    std::cout << std::endl;
  }
    
  for (unsigned int i = 0; i < data.size(); ++i) {

    std::vector<double> p(2 + data_idx, 1);
    
    double info[LM_INFO_SZ];

    // do the fit to segmented linear with fixed breakpoints
    dlevmar_der(&segmented, &jacsegmented, &p[0], &data[i][0], p.size(),
                data[i].size(), 1000, NULL, info, NULL, NULL, &current_bp[0]);
    dlevmar_dif(&segmented, &p[0], &data[i][0], p.size(),
                data[i].size(), 1000, NULL, info, NULL, NULL, &current_bp[0]);

    // calculate residuals from fit
    std::vector<double> residuals;
    calc_residuals(current_bp, p, data[i], residuals);
    
    // sum of squared residuals
    double Q = 0;
    for (std::vector<double>::iterator it = residuals.begin();
         it != residuals.end(); it++) {
      Q += (*it) * (*it);
    }

    // do we have a better fit?
    if (Q < bestQ[i] || bestQ[i] < 0) {
      bestQ[i] = Q;
      if (data_idx > 0) {
        best_bp[i].resize(data_idx);
        std::copy(current_bp.begin()+1, current_bp.begin()+data_idx+1, best_bp[i].begin());
      }
      best_residuals[i] = residuals;
      best_parameters[i] = p;
    }
  }
  
  // try next breakpoint recursively
  if (data_idx > 0) {

    int bp_idx = data_idx - 1;
    bool newRound = false;
    if (current_bp[bp_idx+1] < (data[0].size() - 1 - minYears)) {
      current_bp[bp_idx+1]++;
      newRound = true;
    } else {
      while (bp_idx > 0 && !newRound) {
        --bp_idx;
        if (current_bp[bp_idx+1] <
            (data[0].size() - 1 - minYears *  (data_idx - bp_idx))) {
          current_bp[bp_idx+1]++;
          for (unsigned int i = bp_idx + 1; i < data_idx; ++i) {
            current_bp[i+1] = current_bp[i] + minYears;
          }
          newRound = true;
        }
      }
    }
    if (newRound) {
      grid_search(current_bp, data, bestQ, best_bp, best_residuals, best_parameters,
                  minYears, verbose);
    }
  }
}

void create_bp_vector(unsigned int numBreakpoints, std::vector<double> const &xValues,
                      std::vector<double>& bpVector, unsigned int minYears)
{
  bpVector.clear();
  
  bpVector.push_back(numBreakpoints);

  if (numBreakpoints > 0) {
    unsigned int idx = minYears - 1;
    bpVector.push_back(idx);
    for (unsigned int i = 1; i < numBreakpoints; i++) {
      idx += minYears;
      bpVector.push_back(idx);
    }
  }

  bpVector.insert(bpVector.end(), xValues.begin(), xValues.end());
}

std::string print_results(double const &Q,
                          std::vector<double> const &bp,
                          std::vector<double> const &parameters,
                          std::vector<double> const &residuals,
                          std::vector<double> const &years,
                          std::vector<double> const *data = 0
                          )
{
  std::vector<double> v_Q(1, Q);
  std::vector<std::vector<double> > v_bp(1, bp);
  std::vector<std::vector<double> > v_parameters(1, parameters);
  std::vector<std::vector<double> > v_residuals(1, residuals);
  std::vector<std::vector<double> > *v_data =
    new std::vector<std::vector<double> > (1, *data);

  std::string results =
    print_results(v_Q, v_bp, v_parameters, v_residuals, years, v_data);

  if (v_data != 0) {
    delete v_data;
  }
  
  return results;
}
std::string print_results(std::vector<double> const &Q,
                          std::vector<std::vector<double> > const &bp,
                          std::vector<std::vector<double> > const &parameters,
                          std::vector<std::vector<double> > const &residuals,
                          std::vector<double> const &years,
                          std::vector<std::vector<double> > const *data = 0
                          )
{
  std::stringstream s;
  for (unsigned int i = 0; i < bp.size(); ++i) {
    if (bp.size() > 0) {
      s << "i = " << i << " ";
    }
    s << "Q = " << Q[i] << std::endl;
    if (bp[i].size() > 0) {
      s << "BREAKPOINTS (" << bp[i].size() << ") ";
      for (unsigned int j = 0; j < bp[i].size(); ++j) {
        s << static_cast<unsigned int>
          (years[static_cast<unsigned int>(bp[i][j])]) << " ";
        s << "(" << bp[i][j] << ") ";
      }
      s << std::endl;
    }
    
    s << "PARAMS (" << parameters[i].size() << ") ";
    for (unsigned int j = 0; j < parameters[i].size(); ++j) {
                    s << parameters[i][j] << " ";
    }
    s << std::endl;

    s << "RESIDUALS (" << residuals[i].size() << ") ";
    for (unsigned int j = 0; j < residuals[i].size(); ++j) {
                    s << residuals[i][j] << " ";
    }
    s << std::endl;
    if (data != 0) {
      s << "MODEL VALUES (" << residuals[i].size() << ") ";
      for (unsigned int j = 0; j < residuals[i].size(); ++j) {
        s << (residuals[i][j] + (*data)[i][j]) << " ";
      }
      s << std::endl;
    }
  }
  return s.str();
}

#endif
