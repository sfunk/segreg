#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <algorithm>
#include <math.h>
#include <boost/program_options.hpp>
#include <boost/tokenizer.hpp>
#include <boost/math/special_functions/beta.hpp>
#include <boost/math/distributions/normal.hpp>

#include "segreg.hpp"

namespace po = boost::program_options;

int main(int argc, char* argv[])
{

  unsigned int lowYear, highYear;
  double threshold;
  unsigned int minYears, maxYears;
  int maxBreakpoints;
  unsigned int permutations;
  double alpha;
  std::string filename;
  double gamma = 0.5;
  bool useNormal = false;
  bool ssqr = false;
  bool loglink = false;

  std::string country = "";
  std::string name = "";
    
  bool regression = true;
  unsigned int verbose = 0;

  // read command line parameters
    
  po::options_description main_options
    ("Usage: emergence csv_file [options]... \n\nOptions");

  main_options.add_options()
    ("help,h",
     "produce help message")
    ("verbose,v",
     "produce verbose output")
    ("very-verbose,V",
     "produce very verbose output")
    ("input-file,f", po::value<std::string>(),
     "input file")
    ("name,n", po::value<std::string>(),
     "name for dataset")
    ("low-year,l", po::value<unsigned int>()->default_value(1855),
     "low year")
    ("high-year,H", po::value<unsigned int>()->default_value(2010),
     "high year")
    ("threshold,t", po::value<double>()->default_value(0.05),
     "threshold")
    ("min-years,y", po::value<unsigned int>()->default_value(2),
     "minimum years")
    ("max-years,x", po::value<unsigned int>()->default_value(2),
     "maximum years")
    ("country,c", po::value<std::string>(),
     "choose country")
    ("segmented,s",
     "do segmented regression")
    ("breakpoints,b", po::value<int>(),
     "maximum number of breakpoints")
    ("permutations,p", po::value<unsigned int>()->default_value(1000),
     "number of permutations")
    ("alpha,a", po::value<double>()->default_value(0.05),
     "alpha")
    ("normal,o",
     "assume normally distributed errors")
    ("ssqr,r",
     "calculate sum of squared residuals (and use p-value for comparison)")
    ("loglink,i",
     "use a loglink")
    ;

  po::positional_options_description file_option;
  file_option.add("input-file", -1);

  po::variables_map vm;
    
  try {
    po::store(po::command_line_parser(argc, argv).options(main_options).
              positional(file_option).run(), vm); 
  }
  catch (std::exception& e) {
    std::cerr << "Error parsing command line parameters: " << e.what()
              << std::endl;
    return 1;
  }
  po::notify(vm);

  if (vm.count("verbose")) {
    verbose = 1;
  }
  if (vm.count("very-verbose")) {
    verbose = 2;
  }
  
  if (vm.count("input-file")) {
    filename = vm["input-file"].as<std::string>();
  } else {
    std::cerr << "Error: must specify input file" << std::endl;
    return 1;
  }

  if (vm.count("name")) {
    name = vm["name"].as<std::string>();
  }

  lowYear = vm["low-year"].as<unsigned int>();
  highYear = vm["high-year"].as<unsigned int>();
  
  minYears = vm["min-years"].as<unsigned int>();
  maxYears = vm["max-years"].as<unsigned int>();

  permutations = vm["permutations"].as<unsigned int>();
  alpha = vm["alpha"].as<double>();
  threshold = vm["threshold"].as<double>();
    
  if (!(maxYears > minYears)) {
    std::cerr << "maxYears (" << maxYears << " must be greater than "
              << "minYears (" << minYears << ")" << std::endl;
    return 1;
  }

  if (vm.count("breakpoints")) {
    maxBreakpoints = vm["breakpoints"].as<int>();
  } else {
    maxBreakpoints = static_cast<int>(maxYears / minYears) - 1;
  }

  if (vm.count("segmented")) {
    regression = true;
  }

  if (vm.count("normal")) {
    useNormal = true;
  }

  if (vm.count("ssqr")) {
    ssqr = true;
  }

  if (vm.count("loglink")) {
    loglink = true;
  }

  if (vm.count("country")) {
    country = vm["country"].as<std::string>();
  }

  std::vector<std::vector<std::string> > data;

  // read csv file    
  std::ifstream in(filename.c_str());
  if (!in.is_open()) {
    std::cerr << "Could not open " << filename << std::endl;
    return 1;
  }

  // read data
  typedef boost::tokenizer<boost::escaped_list_separator<char> > Tokenizer;
  boost::escaped_list_separator<char> sep('\\', ',', '\"');

  std::vector<unsigned int> years; //!< years in the data, from late to early
  bool firstLine = true;
  std::string line;

  while (std::getline(in,line))  {
    std::vector<std::string> lineVector;
    Tokenizer tok(line, sep);
    lineVector.assign(tok.begin(), tok.end());

    if (firstLine) {
      for (std::vector<std::string>::iterator it = (lineVector.begin())+1;
           it != lineVector.end(); it++) {
        int i = 0;
        std::istringstream s(*it);
        s >> i;
        years.push_back(i);
      }
      firstLine = false;
    } else {
      data.push_back(lineVector);
    }
  }
    
  int minIndex = 0; //!< index of the lowest year

  for (unsigned int i = 0; i < years.size(); ++i) {
    if (highYear == years[i]) {
      minIndex = i;
    }
  }

  for (std::vector<std::vector<std::string> >::iterator it = data.begin();
       it != data.end(); it++) {

    std::string currentCountry = (*it)[0];
    if (country.length() == 0 || country == currentCountry) {
    
      std::vector<double> xValues;
      std::vector<double> yValues;
      std::vector<double> ySqrtValues;

      std::stringstream line;
      if (name.length() > 0) {
        line << name << ":";
      } else {
        line << currentCountry << ":";
      }

      for (int i = minIndex + maxYears - 1; i > (minIndex - 1); --i) {
        
        int j = 0;
        std::istringstream s((*it)[i+1]); // index 0 is the country
        if (s >> j) {
          xValues.push_back(years[i]);
          double y = 0;
          if (loglink) {
            y = log(j+1); 
          } else {
            y = j;
          }
          yValues.push_back(y);
          if (j > 0) {
            ySqrtValues.push_back(sqrt(y));
          } else {
            ySqrtValues.push_back(gamma);
          }
        }
      }

      if (verbose >= 2) {
        std::cout << "VALUES(x) (" << xValues.size() << ") ";
        for (unsigned int i = 0; i < xValues.size(); ++i) {
          std::cout << xValues[i] << " ";
        }
        std::cout << std::endl;
        
        std::cout << "VALUES(y) (" << yValues.size() << ") ";
        for (unsigned int i = 0; i < yValues.size(); ++i) {
          std::cout << yValues[i] << " ";
        }
        std::cout << std::endl;

        if (verbose >= 2) {
          std::cout << "VALUES(sqrt(y)) (" << ySqrtValues.size() << ") ";
          for (unsigned int i = 0; i < ySqrtValues.size(); ++i) {
            std::cout << ySqrtValues[i] << " ";
          }
          std::cout << std::endl;
        }
      }

      if (yValues.size() > 1) {

        int country_maxBreakpoints;
        
        country_maxBreakpoints =
          std::min(static_cast<int>(xValues.size() / minYears) - 1,
                   maxBreakpoints);

        bool emerged = false, extinct = false;
        if (xValues[0] - minYears >= years[minIndex + maxYears - 1]) {
          line << " emerged in " << xValues[0] << ",";
          emerged = true;
        }
        if (xValues[xValues.size() - 1] + minYears <= years[minIndex]) {
          line << " extinct since " << (xValues[xValues.size() - 1] + 1) << ",";
          extinct = true;
        }

        bool emerging = false;
        bool receding = false;
        if (regression) {
            
          double null_rSquared = -1;
          double alt_rSquared = -1;
          std::vector<double > p_null_rSquared (permutations - 1, -1);

          int low_bp = 0;	// minimum: 0 breakpoints
          int high_bp = country_maxBreakpoints > 0 ? country_maxBreakpoints : -1; // maximum

          bool do_low = true;

          std::vector<std::vector<double> > permuted_yValues;
          std::vector<double> null_bp, null_residuals, null_params;
          std::vector<double> alt_bp, alt_residuals, alt_params;

          while (low_bp < high_bp || high_bp == -1) {
            if (do_low) {
              null_rSquared = -1;
              std::vector<double> null_starting_bp;
              create_bp_vector(low_bp, xValues, null_starting_bp, minYears);

              grid_search(null_starting_bp, yValues, null_rSquared,
                          null_bp, null_residuals, null_params, minYears,
                          verbose);

              if (verbose >= 2) {
                std::cout << print_results(null_rSquared, null_bp, null_params,
                                           null_residuals, xValues, &yValues);
              }
              
              if (high_bp > 0) {
                // don't need to permutations if I
                // have nothing to compare to anyway

                std::vector<double> scaled_residuals;
                if (verbose >= 2) {
                  std::cout << "SCALED RESIDUALS (" << null_residuals.size()
                            << ") ";
                }
                for (unsigned int i = 0; i < null_residuals.size(); ++i) {
                  if (useNormal) {
                    scaled_residuals.push_back(null_residuals[i]);
                  } else {
                    scaled_residuals.push_back(null_residuals[i] / ySqrtValues[i]);
                  }
                  if (verbose >= 2) {
                    std::cout << scaled_residuals[scaled_residuals.size() - 1] << " ";
                  }
                }
                if (verbose >= 2) {
                  std::cout << std::endl;
                }
                
                permuted_yValues.clear();
                for (unsigned int i = 0; i < (permutations - 1); ++i) {
                  permuted_yValues.push_back(scaled_residuals);
                  std::random_shuffle(permuted_yValues[i].begin(), permuted_yValues[i].end());
                  if (verbose >= 2) {
                    std::cout << "PERMUTED (" << permuted_yValues[i].size() << ") ";
                  }
                  for (unsigned int j = 0; j < permuted_yValues[i].size(); ++j) {
                    if (!useNormal) {
                      permuted_yValues[i][j] *= ySqrtValues[j];
                    }
                        
                    permuted_yValues[i][j] += yValues[j];
                    if (verbose >= 2) {
                      std::cout << permuted_yValues[i][j] << " ";
                    }
                  }
                  if (verbose >= 2) {
                    std::cout << std::endl;
                  }
                }

                for (std::vector<double>::iterator it = p_null_rSquared.begin();
                     it != p_null_rSquared.end(); it++) {
                  (*it) = -1;
                }
                std::vector<std::vector<double> > p_null_bp (permutations - 1);
                std::vector<std::vector<double> > p_null_residuals (permutations - 1);
                std::vector<std::vector<double> > p_null_params (permutations - 1);

                create_bp_vector(low_bp, xValues, null_starting_bp, minYears);

                grid_search(null_starting_bp, permuted_yValues, p_null_rSquared,
                            p_null_bp, p_null_residuals, p_null_params,
                            minYears, verbose);
              
                if (verbose >= 2) {
                  std::cout << print_results(p_null_rSquared, p_null_bp,
                                             p_null_params, p_null_residuals,
                                             xValues);
                }
              }
            }

            // now test alternative model (if we do so)
            if (high_bp > 0) {

              alt_rSquared = -1;
            
              std::vector<double> alt_starting_bp;
              create_bp_vector(high_bp, xValues, alt_starting_bp, minYears);

              grid_search(alt_starting_bp, yValues, alt_rSquared,
                          alt_bp, alt_residuals, alt_params,
                          minYears, verbose);
              
              if (verbose >= 2) {
                std::cout << print_results(alt_rSquared, alt_bp,
                                           alt_params, alt_residuals,
                                           xValues, &yValues);
              }
              std::vector<std::vector<double> > p_alt_bp (permutations - 1);
              std::vector<std::vector<double> > p_alt_residuals (permutations - 1);
              std::vector<std::vector<double> > p_alt_params (permutations - 1);
              std::vector<double> p_alt_rSquared (permutations - 1, -1);
              
              create_bp_vector(high_bp, xValues, alt_starting_bp, minYears);
              grid_search(alt_starting_bp, permuted_yValues, p_alt_rSquared,
                          p_alt_bp, p_alt_residuals, p_alt_params,
                          minYears, verbose);
              
              if (verbose >= 2) {
                std::cout << print_results(p_alt_rSquared, p_alt_bp,
                                           p_alt_params, p_alt_residuals,
                                           xValues);
              }
            
              unsigned int p_count = 1;
              double compare = null_rSquared / alt_rSquared;
              for (unsigned int i = 0; i < permutations - 1; ++i) {
                if (verbose >= 2) {
                  std::cout << "COMPARE " << p_null_rSquared[i] / p_alt_rSquared[i]
                            << " " << compare;
                }
                if (p_null_rSquared[i] / p_alt_rSquared[i] >= compare) {
                  if (verbose >= 2) {
                    std::cout << " (+)";
                  }
                  ++p_count;
                }
                if (verbose >= 2) {
                  std::cout << std::endl;
                }
              }
              
              double pValue = p_count / static_cast<double>(permutations);
              if (verbose >= 1) {
                std::cout << low_bp << " vs " << high_bp << ": p=" << pValue
                          << std::endl;
              }

              if (pValue > (alpha / high_bp)) {
                // accepted
                do_low = false;
                --high_bp;
              } else {
                // rejected
                do_low = true;
                ++low_bp;
              }
            } else {
              high_bp = -2;
            }
          }

          std::vector<double> *params;
          std::vector<double> *bp;
          std::vector<double> *residuals;
          std::vector<double> pvalue_slope;
          std::vector<double> pvalue2_slope;

          if (do_low && high_bp > 0) {
            bp = &alt_bp;
            params = &alt_params;
            residuals = &alt_residuals;
          } else {
            bp = &null_bp;
            params = &null_params;
            residuals = &null_residuals;
          }

          if (verbose >= 1) {
            std::cout << "Number of breakpoints: " << bp->size();
            if (bp->size() > 0) {
              std::cout << " ("
                        << xValues[static_cast<unsigned int>((*bp)[0])];
              for (std::vector<double>::iterator it = bp->begin() + 1;
                   it != bp->end(); it++) {
                std::cout << " " << xValues[static_cast<unsigned int>(*it)];
              }
              std::cout << ")";
            }
            std::cout << std::endl;
            
            std::cout << "Parameters: ";
            for (std::vector<double>::iterator it = params->begin();
                 it != params->end(); it++) {
              std::cout << *it << " ";
            }
            std::cout << std::endl;

            std::cout << "Slopes:";
          }

          double paramSum = 0;
          size_t idx = 0;
          pvalue_slope.resize(bp->size() + 1, .0);
          if (!useNormal) {
            pvalue2_slope.resize(bp->size() + 1, .0);
          }
          for (std::vector<double>::iterator it = params->begin() + 1;
               it != params->end(); it++) {
            paramSum += (*it);
            pvalue_slope[idx] = paramSum;
            ++idx;
            
            if (verbose >= 1) {
              std::cout << " " << paramSum;
            }
          }
          
          if (verbose >= 1) {
            std::cout << std::endl;
          }
          
          // calculate slope
          if (ssqr) {

            if (verbose >= 1) {
              std::cout << "SSE:";
            }
            
            double sse = 0;
            int current_bp = 0;
            size_t n = 0;
            unsigned int xSum = 0;
            unsigned int xSquaredSum = 0;
            for (unsigned int i = 0; i < xValues.size(); ++i) {
              ++n;
              xSum += xValues[i];
              xSquaredSum += xValues[i] * xValues[i];
              if (!(current_bp == static_cast<int>(bp->size()) ||
                    i < (*bp)[current_bp])) {
                sse += pow((*residuals)[i], 2);
                std::cout << i << " " << xValues[i] << " " << yValues[i] << " " << sse << std::endl;
                sse =
                  sqrt(sse / (n - 2.0)) /
                  sqrt(xSquaredSum - xSum * xSum / static_cast<double>(n));
                if (verbose) {
                  std::cout << " " << sse;
                }
                double t = pvalue_slope[current_bp] / sse;
                pvalue_slope[current_bp] = 
                  1 -
                  boost::math::ibeta
                  ((n-2) / 2.0, 0.5,
                   (n-2) / (static_cast<double>(n-2) + pow(t, 2)));
                if (!useNormal) {
                  boost::math::normal s; // standard normal distribution
                  pvalue2_slope[current_bp] =
                    1 - boost::math::cdf(s, -fabs(t)) * 2;
                }
                ++current_bp;
                n = 1;
                xSum = xValues[i];
                xSquaredSum = xValues[i] * xValues[i];
                sse = 0;
              }
              sse += pow((*residuals)[i], 2);
              std::cout << i << " " << xValues[i] << " " << yValues[i] << " " << sse << std::endl;
            }
            std::cout << xSquaredSum << " " << xSum * xSum << " " << n
                      << std::endl;
            sse =
              sqrt(sse / (n - 2.0)) /
              sqrt(xSquaredSum - xSum * xSum / static_cast<double>(n));
            double t = pvalue_slope[current_bp] / sse;
            pvalue_slope[current_bp] = 
              1 -
              boost::math::ibeta
              ((n-2) / 2.0, 0.5,
               (n-2) / (static_cast<double>(n-2) + pow(t, 2)));
            if (!useNormal) {
              boost::math::normal s; // standard normal distribution
              pvalue2_slope[current_bp] =
                1 - boost::math::cdf(s, -fabs(t)) * 2;
            }

            if (verbose >= 1) {
              std::cout << " " << sse << std::endl;
              std::cout << "p-values (t):";
              for (std::vector<double>::iterator it = pvalue_slope.begin();
                   it != pvalue_slope.end(); it++) {
                std::cout << " " << 1 - (*it);
              }
              std::cout << std::endl;
              if (!useNormal) {
                std::cout << "p-values (z):";
                for (std::vector<double>::iterator it = pvalue2_slope.begin();
                     it != pvalue2_slope.end(); it++) {
                  std::cout << " " << 1 - (*it);
                }
                std::cout << std::endl;
              }
            }
          }

          // calculate average over that period
          unsigned int periodSum = 0;
          unsigned int lastBp = 0;
          if (bp->size() > 0) {
            lastBp = static_cast<unsigned int>((*bp)[bp->size() - 1]);
          }
          for (unsigned int i = lastBp + 1; i < xValues.size(); ++i) {
            periodSum += yValues[i];
          }

          double caseAvg = periodSum /
            static_cast<double>(xValues.size() - lastBp - 1);
          
          if (caseAvg < 100) caseAvg = 100;

          if (ssqr) {
            if (useNormal) {
              if (pvalue_slope[pvalue_slope.size() - 1] > (1-threshold)) {
                if (paramSum > 0 && !extinct) {
                  emerging = true;
                } else if (paramSum < 0) {
                  receding = true;
                }
              }
            } else {
              if (pvalue2_slope[pvalue2_slope.size() - 1] > (1-threshold)) {
                if (paramSum > 0 && !extinct) {
                  emerging = true;
                } else if (paramSum < 0) {
                  receding = true;
                }
              }
            }
          } else {
            if (paramSum / caseAvg > threshold && !extinct) {
              emerging = true;
            }
            if (paramSum / caseAvg < (-threshold)) {
              receding = true;
            }
          }

          if (emerging) {
            line << " emerging ";
            if (bp->size() > 0) {
              line << "since " << xValues[static_cast<int>((*bp)[bp->size()-1])]
                   << " ";
            }              
          } else if (!extinct) {
            if (receding) {
              line << " receding ";
              if (bp->size() > 0) {
                line << "since " << xValues[static_cast<int>((*bp)[bp->size()-1])]
                     << " ";
              }
            } else {
              line <<  " stable ";
              if (bp->size() > 0) {
                line << "since " << xValues[static_cast<int>((*bp)[bp->size()-1])]
                     << " ";
              }
            }
          }
        }
        std::string outStr = line.str();
        std::cout << outStr.substr(0, outStr.size()-1) << std::endl;
      }
    }
  }
}
