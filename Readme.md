Segmented joinpoint regression
==============================

This implements segmented joinpoint regression as introduced in

Kim et al. (2000) [Permutation tests for joinpoint regression with applications to cancer rates][1]. Statistics in Medicine 19(3):335--351.

An independently developed implementation of this method including a GUI and many more options is available form the [National Cancer Institute][2]. An R package accomplishing a similar task is available under the name [segmented][3].

Requirements
------------

This needs a working C++ compiler and 

- [Levmar][4]
- [Boost][5]

To compile, type *make*. This has been tested under Linux and Mac OS.

Usage
-----

    ./segreg [options] data.csv

where the data is in the format:

- Header row defining time points (e.g., years)
- Subsequent row defining data sets (e.g., data points at the time steps defined in the header)

A list of options can be obtained by giving option *-h*, i.e.

    ./segreg -h

[1]: http://www.ncbi.nlm.nih.gov/pubmed/10649300
[2]: http://surveillance.cancer.gov/joinpoint/
[3]: http://cran.r-project.org/package=segmented
[4]: http://www.ics.forth.gr/~lourakis/levmar/
[5]: http://www.boost.org/
