#------ GNU g++ -----------------------------------------------------

CC         = clang
CPPFLAGS     = -std=c++11 -stdlib=libc++ -Wall -Iinclude -O3

LINKER      = clang
LDFLAGS     = -lstdc++ -lboost_program_options-mt -llevmar -O3 -llapack -lblas

#--------------------------------------------------------------------

MAKEDEPEND  = makedepend
DEPENDFLAGS = -m 

#--------------------------------------------------------------------

SOURCES     = segreg.cpp

CPP_SOURCES = $(filter %.cpp, $(SOURCES))

CPP_OBJECTS = $(patsubst %.cpp,out/%.o,${CPP_SOURCES})

OBJECTS = $(CPP_OBJECTS)

EXEC = segreg

#--------------------------------------------------------------------

all:  bin/$(EXEC)

bin/$(EXEC): $(OBJECTS) 
	@echo ... linking:
	$(LINKER) $(OBJECTS) $(LDFLAGS) $(OPTFLAGS) $(CPPFLAGS) -o $@
	@echo

out/segreg.o : src/segreg.cpp include/segreg.hpp
	@echo ... compiling $<:
	$(CC) -c $(CPPFLAGS) $(OPTFLAGS) $< -o $@
	@echo

depend:
	$(MAKEDEPEND) $(DEPENDFLAGS) src/*.cc

clean:
	@echo ... cleaning $(OBJECTS) $(EXEC)
	@rm -f bin/* out/*
	@echo ... done

cleaner:
	@echo '... cleaning also *~'
	@rm -f bin/* out/* *~ .#*
	@echo ... done

#-------------------------------------------------------------------
# DO NOT DELETE

#out/simulate.o: include/Vertex.hh include/Simulator.hh \
#	include/EpiSimulator.hh include/GillespieSimulator.hh \
